# Cannot allocate memory 汇总

1. 查看报错日志：OpenJDK 64-Bit Server VM warning: INFO: os::commit_memory(0x00000000c0000000, 1073741824, 0) failed; error=‘Out of memory’ (errno=12) 

解决方案：
执行命令free -h 查看内存是否不足，最主要的是 看有没有交换空间 swap
1.创建swapfile：dd if=/dev/zero of=swapfile bs=1024 count=500000

（count=空间大小 of空间名字）
在root权限下，创建swapfile （第一步：创建一个大小为500M的文件。有时会遇到dd命令不识别 可能是你安装过一次了 没事 先把swapfile删除就ok了。）（像/dev/null一样， /dev/zero也是一个伪文件， 但它实际上产生连续不断的null的流（二进制的零流，而不是ASCII型的）。/dev/zero主要的用处是用来创建一个指定长度用于初始化的空文件，就像临时交换文件。）
2.将swapfile设置为swap空间（第二步：把这个文件变成swap文件）：# `mkswap swapfile`

3.启用交换空间，这个操作有点类似于mount操作：`swapon swapfile` （删除交换空间 swapoff swapfile）

至此增加交换空间的操作结束了，可以使用free命令查看swap空间大小是否发生变化
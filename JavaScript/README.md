## 说明：  
### 左右移动插件(穿梭框)
https://github.com/istvan-ujjmeszaros/bootstrap-duallistbox

### datatable
https://ops-coffee.cn/s/5dZx6HrPE1Y4rdxqHTYN-g

### BOM详解
https://www.cnblogs.com/qdhxhz/p/12031925.html

### 表单提交加上传
https://www.layui.com/doc/modules/upload.html

### 禁止事件连续调用
今天遇到个情况,点击事件处理样式调整，离开点击节点恢复样式，结果每次点击在样式上都没有变化，代码如下：
```html
<div>
    <span class="showEye" id="showEye-${row.id}" style="padding: 7px;" onclick="showPassword(${row.id})" onmouseleave="hidePassword(${row.id})">
        <i class="fa fa-eye-slash" data-toggle="tooltip" title="点击查看密码"></i> 
    </span> 
    <span class="showPassword" data-id="${row.id}" id="btnShowPassword-${row.id}" style="position: relative; top: 3px;"> ****** </span>
</div>
```
```javascript
function showPassword(id) {
	$('#showEye-' + id).find('i.fa').addClass('fa-eye').removeClass('fa-eye-slash');
	$.get(vcPrefix + '/toggle_pass?id=' + id, function (res) {
		$('#btnShowPassword-' + id).text(res);
	});
	$('#btnShowPassword-' + id).css('top','0px');
}
function hidePassword(id) {
	$('#showEye-' + id).find('i.fa').removeClass('fa-eye').addClass('fa-eye-slash');
	$('#btnShowPassword-' + id).text(' ****** ');
	$('#btnShowPassword-' + id).css('top', '3px');
}
```
问题出在，每次点击，不仅仅走了showPassword()，也走了hidePassword()方法
通过豆包查询解决方案：
#### 方案一
点击时先移除`onmouseleave`属性，执行完点击事件后在注册添加`onmouseleave`属性
```JavaScript
function showPassword(id) {
	$('#showEye-' + id).removeAttr('onmouseleave');
	$('#showEye-' + id).find('i.fa').addClass('fa-eye').removeClass('fa-eye-slash');
	$.get(vcPrefix + '/toggle_pass?id=' + id, function (res) {
		$('#btnShowPassword-' + id).text(res);
	});
	$('#btnShowPassword-' + id).css('top','0px');
	setTimeout(function () {
		$('#showEye-' + id).attr('onmouseleave','hidePassword(' + id + ')')
	},500);
}
```
#### 方案二
定义变量
```JavaScript
let isClicking = false;
function showPassword(id) {
	isClicking = true;
	$('#showEye-' + id).find('i.fa').addClass('fa-eye').removeClass('fa-eye-slash');
	$.get(vcPrefix + '/toggle_pass?id=' + id, function (res) {
		$('#btnShowPassword-' + id).text(res);
	});
	$('#btnShowPassword-' + id).css('top','0px');
	setTimeout(function () {
		isClicking = false;
	},500);
}
function hidePassword(id) {
    if (!isClicking) {
        $('#showEye-' + id).find('i.fa').removeClass('fa-eye').addClass('fa-eye-slash');
        $('#btnShowPassword-' + id).text(' ****** ');
        $('#btnShowPassword-' + id).css('top', '3px');
    }
}
```

### 正则
[检测正则是否正确](https://jex.im/regulex/#!flags=&re=)
1. 最少包含2个大写字母、2个小写字母、2个数字、2个指定的特殊字符、长度10到20
```
var regex = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*()])(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*()])[\da-zA-Z!@#$%^&*()]{10,20}$/;
```
2. 最少包含1个大写字母、1个小写字母、1个数字、一个指定的特殊字符、长度10到20
```
function checkPassWord(psd){
    var psd=$("#psd").val();
    var contentPattern = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*()])[\da-zA-Z!@#$%^&*()]{10,20}$/;
    var rootPattern=/^((?!root).)*$/;
	
    if(!contentPattern.test(psd)){
        console.log("最少包含1个大写字母、1个小写字母、1个数字、一个指定的特殊字符、长度10到20");
        return "最少包含1个大写字母、1个小写字母、1个数字、一个指定的特殊字符、长度10到20"";
    }
    if(!rootPattern.test(psd)){
        console.log("密码不能包含“root”");
        return "密码不能包含“root”";
    }
}
```
原文：https://blog.csdn.net/chuanxincui/article/details/83820043 

3. 以中文、英文字母开头，可以包含可以包含数字、中文、英文、下划线（_）、短横线（-）,不能以 http:// 和 https:// 开头
```
var regex = /^(?!.*(http[s]:\/\/{0,1}))^[A-Za-z\u4e00-\u9fa5][0-9A-Za-z-_\u4e00-\u9fa5\/(_?:\-)]{1,256}$/;
```
4. 验证IP段
```
var ipv4Regex = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

var ipv4RegexRange = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\/(?:(?:([0-9]))|(?:(1[0-9]))|(?:(2[0-4]))|(?:(3[0-2])))$/,
```

### 1. java加密解密与JavaScript加密解密可以相互配合，及JavaScript加密，用java可以解密；java加密已用JavaScript解密；
  * [JavaScript加密解密](https://gitee.com/tomhat/sources/blob/master/javascript/JavaScript%E5%8A%A0%E5%AF%86%E8%A7%A3%E5%AF%86.md)
  * [java加密解密](https://gitee.com/tomhat/sources/blob/master/javascript/java%E5%8A%A0%E5%AF%86%E8%A7%A3%E5%AF%86.md)

### 2. bootstrap-select简单用法：   
 1. 初始化  `$(select).selectpicker();`  
 2. 单选赋值 `$(select).selectpicker('val', value); `   
 3. 多选选赋值 `$(select).selectpicker('val', values); `  
 4. 取值 `$(select).selectpicker('val'); `  
 5. 销毁 `$(select).selectpicker('destroy'); `  
 6. 动态加载select不可缺少：  
    `$(select).selectpicker('refresh'); `     
    `$(select)selectpicker('render');`      

### 3. bootstrapValidator.js用法：
 * [boostrapValidator用法说明](https://gitee.com/tomhat/sources/blob/master/JavaScript/bootstrapValidator.js.md)
 * [jquery validate用法说明](https://gitee.com/tomhat/sources/blob/master/JavaScript/jquery-validate/README.md)

### 4. layui-upload简单用法:
 * [layui-upload用法示例](https://gitee.com/tomhat/sources/tree/master/javascript/layui-upload)

### 5. webuploader官网示例:  
 * [webuploader](http://fex.baidu.com/webuploader/getting-started.html) 
 * [springboot + webuploader 示例 - spring-file](https://gitee.com/jufeng9878/java-master.git)
## Latest Release via DNF or Yum  

On Fedora:
```
$ sudo dnf install ansible
```

On RHEL and CentOS:
```
$ sudo yum install ansible
```

To enable the Ansible Engine repository, run the following command:
```
$ sudo subscription-manager repos --enable rhel-7-server-ansible-2.6-rpms
```

```
$ git clone https://github.com/ansible/ansible.git
$ cd ./ansible
$ make rpm
$ sudo rpm -Uvh ./rpm-build/ansible-*.noarch.rpm
```

## Latest Releases via Apt (Ubuntu)

Ubuntu builds are available in a PPA here.

To configure the PPA on your machine and install ansible run these commands:
```
$ sudo apt-get update
$ sudo apt-get install software-properties-common
$ sudo apt-add-repository --yes ppa:ansible/ansible
$ sudo apt-get update
$ sudo apt-get install ansible
```

/etc/ansible/hosts
```
[localhost]
192.168.4.140 ansible_password='1234.com'
192.168.4.233 ansible_password='1234.com'
```

FAILED! => {"msg": "Using a SSH password instead of a key is not possible because Host Key checking is enabled and sshpass does not support this.  Please add this host's fingerprint to your known_hosts file to manage this host."}   
修改/etc/ansible/ansible.cfg配置文件
```
host_key_checking = False
```
默认host_key_checking部分是注释的，打开该行的注释，可以实现跳过 ssh 首次连接提示验证部分

## 安装pip
```
yum install -y epel-release
yum install -y python-pip
```
## 升级pip
指定升级到版本，20.0.2，20.3.4
```
python -m pip install --upgrade pip==20.3.4
```
> Python 2 Support
> pip 20.3 was the last version of pip that supported Python 2. Bugs reported with pip which only occur on Python 2.7 will likely be closed as “won’t fix” issues by pip’s maintainers.

## 安装组件 pywinrm,requests
支持win_ping模块依赖

> --default-timeout=100 # 超时时可加入参数

```
pip install pywinrm
pip install requests
```

## 更换pip源
`mkdir -p ~/.pip/pip.conf`   
```
cat >~/.pip/pip.conf<-EOF
[global]
index-url=http://mirrors.aliyun.com/pypi/simple/
 
[install]
trusted-host=mirrors.aliyun.com
EOF
```

## ansible操作Windows
1. 确认WinRM服务已启动：确保目标Windows主机上的WinRM服务正在运行。你可以通过在Windows主机上打开PowerShell并运行以下命令来检查WinRM服务的状态
```powershell
Get-Service -Name WinRM
```
如果服务未运行，可以使用以下命令启动它：
```powershell
Start-Service -Name WinRM
```
2. 配置WinRM监听器：确保WinRM配置了正确的监听器。你可以使用以下命令查看当前的监听器配置
```powershell
winrm e winrm/config/listener
```
3. 检查防火墙设置：确保Windows防火墙允许WinRM流量。你可以在Windows防火墙中创建一个入站规则，允许TCP端口5985（HTTP）和5986（HTTPS）
4. 配置WinRM（Windows远程管理）：确保在Windows上启用了WinRM服务。这可以通过运行winrm quickconfig命令来完成。此外，还需要配置WinRM以允许基本身份验证和不加密的连接，具体命令如下
```powershell
winrm set winrm/config/service/auth '@{Basic="true"}'
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
```
5. Ansible服务主机上操作

5.1 安装pywinrm模块
```
pip install pywinrm
```
5.2 配置Ansible Inventory文件：确保你的Ansible inventory文件中的Windows主机配置正确,例如：
```
[windows]
192.168.7.118 ansible_user=administrator ansible_password=YOUR_PASSWORD ansible_connection=winrm ansible_winrm_server_cert_validation=ignore ansible_port=5986
```
> 忽略SSL证书验证：`ansible_winrm_server_cert_validation=ignore` 选项  
> 增加重试次数: --forks=n 例：`ansible windows -m win_ping --forks=10`